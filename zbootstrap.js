var Zest = Zest || {};
Zest.Bootstrap = Backbone.View.extend({
    CLASSES: {
        disabled: "disabled",
        error:"error"
    },
    makeAlert: function(message, type){
        return this.getAlertTemplate()({message:message,type:type});
    },
    getAlertTemplate: _.once(function(){
        return _.template('<div class="alert alert-<%= type %>"><%= message %></div>');
    }),
    getControlGroup: function(element){
        return $(element).closest(".control-group");
    },
    controlGroupError: function(element, condition){
        this.getControlGroup(element)[condition ? "removeClass" : "addClass"](this.CLASSES.error);
        return condition;
    },
    putMessagesIntoBox:function(arr, box){
        var that=this;
        box.empty();
        _(arr).each(function(item){
            box.append(that.makeAlert(item.message, item.type));
        });
    },
    disable: function(el, condition){
        if(condition==null){
            condition=true;
        }
        el[condition ? "addClass" : "removeClass"](this.CLASSES.disabled);
        return condition;
    },
    isDisabled: function(el){
        return el.hasClass(this.CLASSES.disabled);
    }
});
